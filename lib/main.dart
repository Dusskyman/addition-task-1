import 'package:flutter/material.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: MyHomePage(),
    );
  }
}

class MyHomePage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          children: [
            SizedBox(
              height: 100,
              width: 100,
              child: DecoratedBox(
                decoration: BoxDecoration(
                    color: Colors.green,
                    border: Border.all(),
                    borderRadius: BorderRadius.circular(5)),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.end,
                  mainAxisSize: MainAxisSize.max,
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    SizedBox(
                      width: 90,
                      height: 20,
                    ),
                    SizedBox(
                      width: 90,
                      height: 30,
                      child: DecoratedBox(
                        decoration: BoxDecoration(
                            color: Colors.yellow,
                            border: Border.all(),
                            borderRadius: BorderRadius.circular(5)),
                        child: Padding(
                          padding: EdgeInsets.all(5),
                          child: Text(
                            'Some text',
                          ),
                        ),
                      ),
                    ),
                    SizedBox(
                      width: 90,
                      height: 30,
                      child: DecoratedBox(
                        decoration: BoxDecoration(
                            color: Colors.yellow,
                            border: Border.all(),
                            borderRadius: BorderRadius.circular(5)),
                        child: Align(
                          alignment: Alignment.bottomRight,
                          child: Padding(
                            padding: EdgeInsets.all(5),
                            child: Text(
                              'Some text',
                            ),
                          ),
                        ),
                      ),
                    ),
                  ],
                ),
              ),
            ),
            SizedBox(
              width: 150,
              height: 50,
              child: Material(
                borderRadius: BorderRadius.circular(10),
                color: Colors.green,
                child: DecoratedBox(
                  decoration: BoxDecoration(boxShadow: [
                    BoxShadow(
                      color: Color.fromRGBO(255, 255, 255, 0.4),
                    )
                  ]),
                  child: InkWell(
                    onTap: () {},
                    splashColor: Colors.grey,
                    highlightColor: Colors.transparent,
                    child: Center(child: Text('Hello')),
                  ),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
